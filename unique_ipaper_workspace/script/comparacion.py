import sys
import os
import io
from datetime import datetime
import pandas as pd
import openpyxl
import string
import numpy as np
from datetime import date
from datetime import timedelta
from openpyxl import Workbook
from openpyxl import load_workbook
import xlsxwriter
import xlrd
import re



ws=str(sys.argv[1])
transaccion=str(sys.argv[2])
print("************************")
print("ws : "+ws)

excel_conf_path=os.path.join(ws,"Config.xlsx")
print("************************")
print("Config : "+excel_conf_path)
excel_extract_path=os.path.join(ws,"data_extraccion\\unique_"+transaccion+".xlsx")
#excel_extract_path=os.path.join(ws,"data_extraccion\\unique_110620102440.xlsx")

print("************************")
print("Extracto Excel : "+excel_extract_path)
excel_pared_folder_path=os.path.join(ws,"pared")
excelListDir=os.listdir(excel_pared_folder_path)
excelListDir = [os.path.join(excel_pared_folder_path, basename) for basename in excelListDir]
print("************************")
reporte=os.path.join(ws,"reporte\\reporte.xlsx")
print("reporte salida  : "+ws)

if len(excelListDir)> 1:
    print("************************")
    print("Existe mas de un archivo en la carpeta pared")
    print("Asignado el ultimo archivo para comparacion")
    excel_pared_path=max(excelListDir,key=os.path.getctime)
else:
    print("************************")
    print("Asignando pared para comparacion")
    excel_pared_path=os.path.join(excel_pared_folder_path,excelListDir[0])

print("************************")
print("pared : "+excel_pared_path)


parametros=pd.read_excel(excel_conf_path,sheet_name="Constants")
print("************************")
print("generando lista parametros")
dic_parametros={}
list_parametros=[]
list_precios=[]
for i,a in parametros.iterrows():
    dic_parametros[parametros.at[i,"Name"]]=parametros.at[i,"Value"]
    list_parametros.append(parametros.at[i,"Value"])
    if "precio." in parametros.at[i,"Name"]:
        list_precios.append(parametros.at[i,"Name"])

print(list_precios)

print("************************")
print("Cargando Pared y filtrando columnas segun parametros")
pared=pd.read_excel(excel_pared_path,sheet_name="Condicom catalogo C6 20 20",dtype=str)
pared=pared.loc[:,list_parametros]

print("************************")
print("Cargando extracto de Ipaper")
extracto=pd.read_excel(excel_extract_path,sheet_name="reporte",dtype=str)

#print(extracto)
for i,a in extracto.iterrows():
    ##Arreglando precion
    extracto.at[i,"precio"]=extracto.at[i,"precio"].split(" ")[0]
    if extracto.at[i,"codigo"] in pared[dic_parametros["codigo"]].tolist():
        print("codigo : "+ str(extracto.at[i,"codigo"]) +" encontrado en pared")
        extracto.at[i,"comparativa-"+dic_parametros["codigo"]]="OK"  
        for j,b in pared.iterrows():
            if extracto.at[i,"codigo"]==pared.at[j,dic_parametros["codigo"]]:
                #si esta el parametro producto validamos 
                if "producto" in dic_parametros:
                    if extracto.at[i,"producto"].strip()!=pared.at[j,dic_parametros["producto"]]:
                        print("validacion producto -> NO SIMILARES")
                        extracto.at[i,"comparativa-"+dic_parametros["producto"]]="no coincide con PARED"
                    else:
                        extracto.at[i,"comparativa-"+dic_parametros["producto"]]="OK"
                
                ##precio validacion
                if "precio.normal" in dic_parametros or "precion.oferta" in dic_parametros:
                    formatPrice={} 
                    try:
                        pricex=float(extracto.at[i,"precio"].split(" ")[0].strip())
                        pricex=float("{:.2f}".format(pricex))

                        for a in list_precios: 
                            pric=float(pared.at[j,dic_parametros[a]])
                            pric=float("{:.2f}".format(pric))
                            formatPrice[a]=str(pric)

                    
                        for x in formatPrice:
                            if str(pricex)!=formatPrice[x]:
                                print("validacion "+ x +" -> NO SIMILARES")
                                extracto.at[i,"comparativa-"+dic_parametros[x]]=np.nan
                            else:
                                extracto.at[i,"comparativa-"+dic_parametros[x]]="OK"

                        formatPrice={}   
                    except:
                        print("formato de fecha no valido")
            
                ##valido cometario
                
                if "descripcion" in dic_parametros:
                    if not pd.isnull(extracto.at[i,"descripcion"]):
                        if extracto.at[i,"descripcion"].strip() != pared.at[j,dic_parametros["descripcion"]]:
                           print("validacion descripcion -> NO SIMILARES")
                           extracto.at[i,"comparativa-"+dic_parametros["descripcion"]]="no coincide con PARED"
                        else:
                            extracto.at[i,"comparativa-"+dic_parametros["descripcion"]]="OK"
                    else:
                        extracto.at[i,"comparativa-"+dic_parametros["descripcion"]]="OK"

    else:
        print("codigo : "+ str(extracto.at[i,"codigo"]) +" no se encuentra en pared")
        extracto.at[i,"comparativa-"+dic_parametros["codigo"]]="No se encuentra en PARED"       





#print(extracto.loc[['comparativa-Precio Normal','comparativa-Precio Oferta']])
#print(extracto.columns.values)
#print(extracto)
extracto.to_excel(reporte,sheet_name="reporte")